﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Token.Sample.Models
{
  public class Usuario
  {
    public int Id { get; set; }
    public string Nome { get; set; }

    //[IgnoreDataMember]
    public string Senha { get; set; }
    public string Login { get; set; }

  }
}
