﻿using FluentMigrator;
using FluentMigrator.Runner.Extensions;

namespace PortalDataGovSap.Db.Migrations
{
    [Migration(20180407205700)]
    public class _20180407205700_CriarTabelaDeTabelaUsuario : Migration
    {

        internal static string TabelaDeUsuario = "Usuario";
        internal static int CARGO_DE_ADMINISTRADOR = -1;
        internal static int ID_DO_ADMIN = -1;

        public override void Up()
        {
            Create.Table(TabelaDeUsuario)
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Login").AsString(255).NotNullable().WithDefaultValue(string.Empty).Indexed()
                .WithColumn("Nome").AsString(255).NotNullable().WithDefaultValue(string.Empty)
                .WithColumn("Sobrenome").AsString(255).NotNullable().WithDefaultValue(string.Empty)
                .WithColumn("Telefone").AsString(50).NotNullable().WithDefaultValue(string.Empty)
                .WithColumn("Senha").AsString(255).NotNullable().WithDefaultValue(string.Empty)
                .WithColumn("DataDeCadastro").AsDateTimeOffset().Nullable()
                .WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("Bloqueado").AsBoolean().NotNullable().WithDefaultValue(string.Empty)
                .WithColumn("CargoDoUsuarioId").AsInt32().NotNullable()
                .ForeignKey(_20180407205000_CriarTabelaDeTabelaCargoDoUsuario.TabelaDeCargoDoUsuario,"Id")
                .WithColumn("Email").AsString(200).NotNullable().WithDefaultValue(false)
                .WithColumn("Cpf").AsString(13).NotNullable().WithDefaultValue(string.Empty);

            Insert.IntoTable(TabelaDeUsuario).WithIdentityInsert()
                .Row(new
                {
                    Id = ID_DO_ADMIN,
                    Login = "admin",
                    Senha = "$2b$10$vUVNm0FqYRoxkSt1qg6ye.7cb5gLHrInKT3/d7dxAWPaRi0ERs.92",
                    Nome  = "Administrador",
                    Sobrenome = "Diego",
                    Telefone = "123",
                    Email = "diego.alves.muniz2@gmail.com",
                    Cpf = "123",
                    CargoDoUsuarioId = CARGO_DE_ADMINISTRADOR
                });



        }

        public override void Down()
        {
            Delete.Table(TabelaDeUsuario);
        }
    }
}
