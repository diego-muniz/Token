﻿using FluentMigrator;
using FluentMigrator.Runner.Extensions;

namespace PortalDataGovSap.Db.Migrations
{
    [Migration(20180407205000)]
    public class _20180407205000_CriarTabelaDeTabelaCargoDoUsuario : Migration
    {

        internal static string TabelaDeCargoDoUsuario = "CargoDoUsuario";
        internal static int CARGO_DE_ADMINISTRADOR = -1;

        public override void Up()
        {
            Create.Table(TabelaDeCargoDoUsuario)
                 .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                 .WithColumn("Nome").AsString(255).NotNullable().WithDefaultValue(string.Empty)
                ;

            Insert.IntoTable(TabelaDeCargoDoUsuario).WithIdentityInsert()
                .Row(new {Id = CARGO_DE_ADMINISTRADOR, Nome = "Gerente"});

            Insert.IntoTable(TabelaDeCargoDoUsuario)
                .Row(new { Nome = "Gerente" })
                .Row(new { Nome = "Funcionario" });
        }

        public override void Down()
        {
            Delete.Table(TabelaDeCargoDoUsuario);
        }
    }
}
