﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Token.Sample.Models
{
  public class DbTokenContext : DbContext
  {
    public DbTokenContext(DbContextOptions dbContextOptions) : base(dbContextOptions) { }

    public DbSet<Usuario> Usuario { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);
    }

    public void TestarTodasTabelas()
    {
      var temUsuario = Usuario.Take(1).ToList();
    }
  }
}
