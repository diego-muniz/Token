﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Token.Sample.Models;
using Token.Sample.Models.Authenticacao;
using Microsoft.AspNetCore.Authorization;

namespace Token.Sample.Controllers
{
  [Route("api/[controller]")]
  public class UsuarioController : Controller
  {

    private readonly DbTokenContext _db;
    private readonly TokenConfigurations _tokenConfigurations;

    public UsuarioController(DbTokenContext db, TokenConfigurations tokenConfigurations)
    {
      _db = db;
      _tokenConfigurations = tokenConfigurations;
    }

    [AllowAnonymous]
    [HttpPost]
    public object Post([FromBody]Usuario usuario)
    {
      try
      {
        var usuarioExiste = _db.Usuario.FirstOrDefault(x => x.Login.Equals(usuario.Login));
        if (usuarioExiste != null)
          return BadRequest("O Usuario já existe. Tente novamente !");

        usuario.Senha = BCrypt.Net.BCrypt.HashPassword(usuario.Senha);

        _db.Usuario.Add(usuario);
        _db.SaveChanges();
        return Ok("Usuario cadastrado com sucesso !");
      }
      catch (Exception ex)
      {
        return BadRequest(ex.Message);
      }
    }

    [AllowAnonymous]
    [HttpPost("novoUsuario")]
    public object NovoUsuario([FromBody]Usuario usuario)
    {
      try
      {
        var usuarioExiste = _db.Usuario.FirstOrDefault(x => x.Login.Equals(usuario.Login));
        if (usuarioExiste != null)
          return BadRequest("O Usuario já existe. Tente novamente !");

        usuario.Senha = BCrypt.Net.BCrypt.HashPassword(usuario.Senha);

        _db.Usuario.Add(usuario);
        _db.SaveChanges();
        return Ok("Usuario cadastrado com sucesso !");
      }
      catch (Exception ex)
      {
        return BadRequest(ex.Message);
      }
    }

  }
}