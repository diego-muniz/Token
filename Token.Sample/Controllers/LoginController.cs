﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Token.Sample.Models;
using Token.Sample.Models.Authenticacao;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Token.Sample.Controllers
{
  [Route("api/[controller]")]
  public class LoginController : Controller
  {

    private readonly DbTokenContext _db;
    private readonly TokenConfigurations _tokenConfigurations;

    public LoginController(DbTokenContext db, TokenConfigurations tokenConfigurations)
    {
      _db = db;
      _tokenConfigurations = tokenConfigurations;
    }

    [AllowAnonymous]
    [HttpPost]
    public object Post([FromBody]UsuarioESenha usuario)
    {
      var usuarioNaoConfere = new ResultadoDaAutenticacao
      {
        Message = "Usuário ou Senha não confere !"
      };

      var usuarioOuSenhaNaoInformado = string.IsNullOrWhiteSpace(usuario?.UserName) ||
                                       string.IsNullOrWhiteSpace(usuario?.Password);

      if (usuarioOuSenhaNaoInformado)
        return BadRequest(usuarioNaoConfere);

      var usuarioDoBanco = _db.Usuario
                              .FirstOrDefault(x => x.Login == usuario.UserName);

      var usuarioNaoEncontrado = usuarioDoBanco == null;

      if (usuarioNaoEncontrado || !BCrypt.Net.BCrypt.Verify(usuario.Password, usuarioDoBanco.Senha))
        return BadRequest(usuarioNaoConfere);

      return ObterToken(usuarioDoBanco);
    }

    private object ObterToken(Usuario usuarioDoBanco)
    {
      var dataCriacao = DateTime.Now;
      var dataExpiracao = dataCriacao.AddMinutes(_tokenConfigurations.TokenLifetimeInMinutes);

      var identity = new ClaimsIdentity(
          new GenericIdentity(usuarioDoBanco.Login, "Login"),
            new[]
            {
              new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
              new Claim(JwtRegisteredClaimNames.Iss, _tokenConfigurations.Issuer),
              new Claim(JwtRegisteredClaimNames.Aud, _tokenConfigurations.Audience),
              new Claim(JwtRegisteredClaimNames.Exp, dataExpiracao.Ticks.ToString()),
              new Claim("name", $"{usuarioDoBanco.Nome}"),
              new Claim(JwtRegisteredClaimNames.GivenName, usuarioDoBanco.Nome),
            }
      );

      var handler = new JwtSecurityTokenHandler();
      var siginCredentials =
          new SigningCredentials(
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenConfigurations.SymmetricSecurityKey)),
            SecurityAlgorithms.HmacSha256);

      var securityToken = handler.CreateToken(new SecurityTokenDescriptor
      {
        Issuer = string.Empty,
        Audience = string.Empty,
        SigningCredentials = siginCredentials,
        Subject = identity,
        NotBefore = dataCriacao,
        Expires = dataExpiracao
      });

      var token = handler.WriteToken(securityToken);

      return new ResultadoDaAutenticacao
      {
        Authenticated = true,
        Created = dataCriacao,
        Expiration = dataExpiracao,
        AccessToken = token,
        Message = "OK"
      };

    }

    public class UsuarioESenha
    {
      public string UserName { get; set; }
      public string Password { get; set; }
    }

    public class ResultadoDaAutenticacao
    {
      public bool Authenticated { get; set; }
      public DateTime Created { get; set; }
      public DateTime Expiration { get; set; }
      public string AccessToken { get; set; }
      public string Message { get; set; }
    }
  }
}