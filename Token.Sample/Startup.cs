﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Token.Sample.Models.Authenticacao;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Token.Sample.Models;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Token.Db;

namespace Token.Sample
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      ConfigurationAuthentication(services);

      services.AddMvc();

      var connectionString = Configuration.GetConnectionString("TokenSample");

        //#if DEBUG
        //MigrationRunner.Down(connectionString);
        MigrationRunner.Up(connectionString);
        //#endif


            services.AddDbContext<DbTokenContext>(
        options => options.UseSqlServer(connectionString));

      var logger = services
          .BuildServiceProvider()
          .GetService<DbTokenContext>();

      logger.TestarTodasTabelas();

    }

    #region Configure Token
    private void ConfigurationAuthentication(IServiceCollection services)
    {
      var tokenConfigurations = new TokenConfigurations();

      new ConfigureFromConfigurationOptions<TokenConfigurations>(
          Configuration.GetSection("TokenConfigurations"))
          .Configure(tokenConfigurations);

      services.AddSingleton(tokenConfigurations);

      services.AddAuthentication(authOptions =>
      {
        authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
      }).AddJwtBearer(bearerOptions =>
      {
        var paramsValidation = bearerOptions.TokenValidationParameters;
        paramsValidation.IssuerSigningKey =
          new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenConfigurations.SymmetricSecurityKey));

        paramsValidation.ValidAudience = tokenConfigurations.Audience;
        paramsValidation.ValidIssuer = tokenConfigurations.Issuer;
        paramsValidation.ValidateIssuerSigningKey = true;
        paramsValidation.ValidateLifetime = true;
        paramsValidation.ClockSkew = TimeSpan.Zero;

        bearerOptions.Events = new JwtBearerEvents()
        {
          OnTokenValidated = OnTokenValidated,
          OnAuthenticationFailed = OnAuthenticationFailed
        };
      });

      services.AddAuthorization(auth =>
      {
        auth.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
            .RequireAuthenticatedUser()
            .Build();
      });
    }

    static async Task OnTokenValidated(TokenValidatedContext arg)
    {
      Debug.Write(arg);
    }

    static async Task OnAuthenticationFailed(AuthenticationFailedContext context)
    {
      Debug.Write(context);
    } 
    #endregion

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      loggerFactory.AddConsole(Configuration.GetSection("Logging"));
      loggerFactory.AddDebug();

      app.Use(async (context, next) =>
      {
        await next();

        if (context.Response.StatusCode == 404 &&
            !Path.HasExtension(context.Request.Path.Value) &&
            !context.Request.Path.Value.StartsWith("/api")) {
                context.Request.Path = "/index.html";
                context.Response.StatusCode = 200; 
                await next();
           }
      });

      app.UseDefaultFiles();
      app.UseStaticFiles();

      app.UseMvc();
    }
  }
}
